# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'popup.ui'
#
# Created: Sat May  6 22:54:46 2017
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QPixmap

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow_Pop(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(803, 562)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.img = QtGui.QLabel(self.centralwidget)
        self.img.setGeometry(QtCore.QRect(10, 0, 120, 120))
        self.img.setObjectName(_fromUtf8("img"))
        self.uni_nm_lb = QtGui.QLabel(self.centralwidget)
        self.uni_nm_lb.setGeometry(QtCore.QRect(160, 0, 591, 71))
        self.uni_nm_lb.setStyleSheet(_fromUtf8("font-size:26px;\n"
                                               "color:#1b73b8;"))
        self.uni_nm_lb.setObjectName(_fromUtf8("uni_nm_lb"))
        self.label_x = QtGui.QLabel(self.centralwidget)
        self.label_x.setGeometry(QtCore.QRect(320, 60, 241, 61))
        self.label_x.setStyleSheet(_fromUtf8("font-size:20px;\n"
                                             "color:#0e8049;"))
        self.label_x.setObjectName(_fromUtf8("label_x"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(290, 100, 231, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 210, 200, 31))
        self.label.setStyleSheet(_fromUtf8("font-size:20px;"))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 250, 111, 31))
        self.label_2.setStyleSheet(_fromUtf8("font-size:20px;"))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 290, 111, 31))
        self.label_3.setStyleSheet(_fromUtf8("font-size:20px;"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(130, 340, 46, 13))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.bck = QtGui.QPushButton(self.centralwidget)
        self.bck.setText("<-- Back <--")
        self.bck.setStyleSheet(_fromUtf8("font-size:16px; color:purple; font-weight:bolder;"))
        self.bck.setGeometry(QtCore.QRect(10, 130, 120, 35))


        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.img.setPixmap(QPixmap("logo_1.png"))
        self.uni_nm_lb.setText(_translate("MainWindow", "Abbottabad University Of Science And Technology", None))
        self.label_x.setText(_translate("MainWindow", "Plagiarism Checker", None))
        self.label.setText(_translate("MainWindow", "Plagiarised % :", None))
        self.label_2.setText(_translate("MainWindow", "Path of File :", None))
        self.label_3.setText(_translate("MainWindow", "Similar Text :", None))
        self.label_4.setText(_translate("MainWindow", "TextLabel", None))

