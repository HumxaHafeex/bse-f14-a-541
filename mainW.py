# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainW.ui'
#
# Created: Sat May  6 12:55:29 2017
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import sys
from PyQt4.QtGui import *
import os

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(803, 562)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.img = QtGui.QLabel(self.centralwidget)
        self.img.setGeometry(QtCore.QRect(10, 0, 120,120))
        self.img.setObjectName(_fromUtf8("img"))
        self.uni_nm_lb = QtGui.QLabel(self.centralwidget)
        self.uni_nm_lb.setGeometry(QtCore.QRect(160, 0, 591, 71))
        self.uni_nm_lb.setStyleSheet(_fromUtf8("font-size:26px;\n"
"color:#1b73b8;"))
        self.uni_nm_lb.setObjectName(_fromUtf8("uni_nm_lb"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(320, 60, 241, 61))
        self.label_3.setStyleSheet(_fromUtf8("font-size:20px;\n"
"color:#0e8049;"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.f_sel = QtGui.QPushButton(self.centralwidget)
        self.f_sel.setGeometry(QtCore.QRect(20, 360, 101, 31))
        self.f_sel.setStyleSheet(_fromUtf8("font-size:16px;\n"
"color:purple;"))
        self.f_sel.setObjectName(_fromUtf8("f_sel"))
        self.f_name = QtGui.QLabel(self.centralwidget)
        self.f_name.setGeometry(QtCore.QRect(130, 370, 500, 20))
        self.f_name.setStyleSheet(_fromUtf8("font-size:16px;"))
        self.f_name.setObjectName(_fromUtf8("f_name"))
        self.text_box = QtGui.QPlainTextEdit(self.centralwidget)
        self.text_box.setGeometry(QtCore.QRect(10, 120, 781, 181))
        self.text_box.setStyleSheet(_fromUtf8("font-size:14px;"))
        self.text_box.setObjectName(_fromUtf8("text_box"))
        self.w_btn = QtGui.QPushButton(self.centralwidget)
        self.w_btn.setGeometry(QtCore.QRect(605, 340, 121, 21))
        self.t_words = QtGui.QLabel(self.centralwidget)
        self.t_words.setGeometry(QtCore.QRect(605, 310, 200, 21))
        self.t_words.setStyleSheet(_fromUtf8("font-size:16px;\n"
"color:crimson;"))
        self.t_words.setObjectName(_fromUtf8("t_words"))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(290, 100, 231, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.start_chk = QtGui.QPushButton(self.centralwidget)
        self.start_chk.setGeometry(QtCore.QRect(320, 430, 191, 51))
        self.start_chk.setStyleSheet(_fromUtf8("font-size:20px;\n"
"color:#1b73b8;"))
        self.start_chk.setObjectName(_fromUtf8("start_chk"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.img.setPixmap(QPixmap("logo_1.png"))
        self.uni_nm_lb.setText(_translate("MainWindow", "Abbottabad University Of Science And Technology", None))
        self.label_3.setText(_translate("MainWindow", "Plagiarism Checker", None))
        self.f_sel.setText(_translate("MainWindow", "Choose File ", None))
        self.f_name.setText(_translate("MainWindow", "No file choosen", None))
        self.t_words.setText(_translate("MainWindow", "Total Words: ", None))
        self.w_btn.setText(_translate("MainWindow","Get Total  Words",None))
        self.start_chk.setText(_translate("MainWindow", "Check Plagiarism", None))
        self.actionExit.setText(_translate("MainWindow", "Exit", None))

