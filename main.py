from PIL import Image
from PyQt4 import QtGui ,uic
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from mainW import Ui_MainWindow
from pop import  Ui_MainWindow_Pop
import sys
from shutil import copyfile, copy
import nltk
import string
import os
import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer
from  nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
import scipy
from sklearn import preprocessing, metrics, cross_validation

class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_MainWindow()
        self.setWindowTitle('AUST - Plagirism Checker')
        self.ui.setupUi(self)

        self.ui.text_box.setFocus()
        self.ui.w_btn.clicked.connect(self.w_count)
        self.ui.f_sel.clicked.connect(self.getfile)
        self.ui.start_chk.clicked.connect(self.check)


    def w_count(self):
        x = self.ui.text_box.toPlainText()
        self.ui.t_words.setText("Total Words: "+ str(len(x.replace(" ",""))))
        print(len(x.replace(" ","")))

    def getfile(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file','', "Doc Files (*.txt *.pdf)")
        self.ui.f_name.setText(fname)
        if(fname==''):
            msgbox = QMessageBox()
            msgbox.question(self, 'Error',
                            "No File Selected"
                            , QtGui.QMessageBox.Ok)
        else:
            try:
                copy(fname, "database")
            except:
                print(Exception)
                msgbox = QMessageBox()
                msgbox.question(self, 'Error',"File with this name Exist in Database!\nPlease Select File with different Name or Some New File."
                                                    ,QtGui.QMessageBox.Ok)



    def check(self ):
        self.ui2= Ui_MainWindow_Pop()
        self.setWindowTitle('Results')
        self.ui2.setupUi(self)
        self.ui2.bck.clicked.connect(self.back)

        userinput1 = input("Enter file name:")
        myfile1 = open(userinput1).read()
        stop_words = set(stopwords.words("english"))
        word1 = nltk.word_tokenize(myfile1)
        filtration_sentence = []
        for w in word1:
            word = word_tokenize(myfile1)
            filtered_sentence = [w for w in word if not w in stop_words]
            print(filtered_sentence)

        userinput2 = input("Enter file name:")
        myfile2 = open(userinput2).read()
        stop_words = set(stopwords.words("english"))
        word2 = nltk.word_tokenize(myfile2)
        filtration_sentence = []
        for w in word2:
            word = word_tokenize(myfile2)
            filtered_sentence = [w for w in word if not w in stop_words]
            print(filtered_sentence)

        stemmer = nltk.stem.porter.PorterStemmer()
        remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

        def stem_tokens(tokens):
            return [stemmer.stem(item) for item in tokens]

        '''remove punctuation, lowercase, stem'''

        def normalize(text):
            return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

        vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')

        def cosine_sim(myfile1, myfile2):
            tfidf = vectorizer.fit_transform([myfile1, myfile2])
            return ((tfidf * tfidf.T).A)[0, 1]

        print(cosine_sim(myfile1, myfile2))


    def back(self):
        self.ui = Ui_MainWindow()
        self.setWindowTitle('AUST - Plagirism Checker')
        self.ui.setupUi(self)
        self.ui.f_sel.clicked.connect(self.getfile)
        self.ui.start_chk.clicked.connect(self.check)
        self.ui.w_btn.clicked.connect(self.w_count)

        print('clicked')


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    myapp = StartQT4()
    myapp.show()
    app.exec_()
    myapp.close()

